
<!-- README.md is generated from README.Rmd. Please edit that file -->

# refineR

The goal of refineR is to provide a way to apply long Open Refine
history files to a dataset, directly through R.

The package is still underdevelopment, and currently only a limited set
of functionalities has been implemented. Notably, only `text_transform`
and `mass_edit` operations are supported at the moment.

Package web site @ <https://krpack.gitlab.io/refiner>.

## Installation

To install through git, use

``` r
# install.packages("devtools")
devtools::install_git("https://gitlab.com/krpack/refiner.git")
```

To install from a local folder, clone the repository and use

``` r
# install.packages("devtools")
devtools::install(path_to_package) 
```

## Example

The main function of the package is the `apply_history_file()` function.
This function takes as arguments:

  - `json_path` path to the open refine history file, containing all of
    the operations to perform.
  - `dataset` dataset to which the workflow should be applied.
  - `op_indexs`, default to `NULL`, optional subset of operations to
    perform, as a vector (ie. `op_indexs = c(1,2,3,4,5)`).

Here is an example on how to use it:

``` r

dataset <- read.csv('dataset_to_preprocess.csv')
library(refineR)
preprocessed_dataset <- apply_history_file(json_path = 'or_workflow.json',
                                           dataset   = dataset)
```

For a more detailed example of the functioning of this package, see the
[vignette](https://krpack.gitlab.io/refiner/articles/example_usage.html).
